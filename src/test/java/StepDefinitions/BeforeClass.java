package StepDefinitions;

import com.codeborne.selenide.Configuration;
import io.cucumber.java.Before;
import static com.codeborne.selenide.Browsers.FIREFOX;

public class BeforeClass {

    @Before
    public static void beforeClass() {
        Configuration.browser = FIREFOX;
        Configuration.startMaximized = true;
        Configuration.timeout = 3000;
//        Configuration.headless = true;
    }

}
