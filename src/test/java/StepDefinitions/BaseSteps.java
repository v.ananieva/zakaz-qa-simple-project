package StepDefinitions;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;

import io.cucumber.java.en.Given;
import org.openqa.selenium.By;


public class BaseSteps {

    @Given("Подождать \"(.*?)\" секунд")
    public void wait(int sec) {
        sleep(sec*1000);
        System.out.println("Привет, я метод!");
        System.out.print("Я делаю: ");
        System.out.println("Подождать " + sec + " секунд");
    }

//    =========================================================

    @Given("Пользователь открывает страницу ордермана")
    public void goToOrdermanURL() {
        String url = "https://orderman.test.zakaz.ua";
        open(url);
    }

    @Given("Пользователь вводит логин")
    public void enterName() {
        String name = "380935555008";
        String xPath = "//*[@id='id_user_id']";
        $(By.xpath(xPath)).setValue(name);
    }

    @Given("Пользователь вводит пароль")
    public void enterPass() {
        String pass = "111111";
        String xPath = "//*[@id='id_password']";
        $(By.xpath(xPath)).setValue(pass);
    }

    @Given("Пользователь нажимает на кнопку")
    public void clickLogInButton() {
        String xPath = "//*[@value='Log in']";
        $(By.xpath(xPath)).click();
    }

    @Given("Пользователь попадает в ордерман")
    public void checkOrderLink() {
        String xPath = "//*[@id='orders-search-link']";
        $(By.xpath(xPath)).shouldBe(visible);
    }

//    ===============================================

    @Given("Пользователь открывает страницу \"(.*?)\"")
    public void goToURL(String url) {
        open(url);
    }

    @Given("Пользователь вводит \"(.*?)\" в поле \"(.*?)\"")
    public void enterName(String value, String field) {
        String xPath = "//*[@id='" + field + "']"; // //*[@id='id_user_id']
        $(By.xpath(xPath)).setValue(value);
    }

    @Given("Пользователь нажимает на кнопку \"(.*?)\"")
    public void clickOnButton(String button) {
        String xPath = "//*[@value='" + button + "']";
        $(By.xpath(xPath)).click();
    }

    @Given("Пользователь видит вкладку \"(.*?)\"")
    public void checkLink(String tab) {
        String xPath = "//*[@id='" + tab + "']";
        $(By.xpath(xPath)).shouldBe(visible);
    }
}
