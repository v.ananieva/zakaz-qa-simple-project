package StepDefinitions;

import io.cucumber.java.After;
import static com.codeborne.selenide.Selenide.*;

public class AfterClass {

    @After
    public void afterTest() {
        closeWebDriver();
    }
}
